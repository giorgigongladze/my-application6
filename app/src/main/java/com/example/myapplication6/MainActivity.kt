package com.example.myapplication6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.myapplication6.adapters.ViewPagerFragmentAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var ViewPager : ViewPager2
    private lateinit var TabLayout : TabLayout
    private lateinit var adapter : ViewPagerFragmentAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        ViewPager.adapter = adapter
        TabLayoutMediator(TabLayout,ViewPager){tab,position ->
            tab.text = "tab" + (position + 1)

        } .attach()
    }

    fun init() {
        ViewPager = findViewById(R.id.ViewPager)
        TabLayout = findViewById(R.id.TabLayout)
        adapter = ViewPagerFragmentAdapter(this)
    }
}